﻿using System;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Wrapper.RabbitInfrastructure
{
    public class ConnectionFactory : RabbitMQ.Client.ConnectionFactory
    {
        public ConnectionFactory(Uri uri)
        {
            Uri = uri;
            RequestedConnectionTimeout = 10000;
            RequestedHeartbeat = 30;
            AutomaticRecoveryEnabled = true;
            TopologyRecoveryEnabled = true;
            NetworkRecoveryInterval = TimeSpan.FromSeconds(10);
        }
    }
}
