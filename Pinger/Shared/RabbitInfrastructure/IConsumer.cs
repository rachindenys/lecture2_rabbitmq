﻿using System;
using RabbitMQ.Client.Events;

namespace Wrapper.RabbitInfrastructure
{
    public interface IConsumer : IDisposable
    {
        void SetAcknowledge(ulong deliveryTag, bool processed);

        void Connect(ConsumerSettings settings, EventHandler<BasicDeliverEventArgs> received);
    }
}