﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Wrapper.RabbitInfrastructure
{
    public class RabbitMQWrapper : IDisposable
    {
        private readonly IConsumer _consumer;
        private readonly IProducer _producer;
        public RabbitMQWrapper(
            IConsumer consumer,
            IProducer producer
            )
        {
            _consumer = consumer;
            _producer = producer;
        }

        public void ListenQueue(ConsumerSettings settings, EventHandler<BasicDeliverEventArgs> received)
        {
            _consumer.Connect(settings, received);
        }

        public void SendMessageToQueue(ProducerSettings settings, string message)
        {
            _producer.Send(message, settings);
        }

        public void SetAcknowledge(ulong deliveryTag, bool processed)
        {
            _consumer.SetAcknowledge(deliveryTag, processed);
        }

        public void Dispose()
        {
            _consumer.Dispose();
        }
    }
}
