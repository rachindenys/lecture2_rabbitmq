﻿using System;

namespace Wrapper.RabbitInfrastructure
{
    public interface IProducer
    {
        void Send(string message, ProducerSettings settings, string type = null);

    }
}