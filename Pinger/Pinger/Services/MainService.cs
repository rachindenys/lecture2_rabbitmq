﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pinger.Services
{
    class MainService
    {
        private readonly MessageService _messageService;

        public MainService(
            MessageService messageService
            )
        {
            _messageService = messageService;
        }
        public void Start()
        {
            Console.WriteLine($"[{DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")}] Pinger Started!");
        }
    }
}
