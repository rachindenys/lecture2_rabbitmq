﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Wrapper.RabbitInfrastructure;

namespace Pinger.Services
{
    class MessageService
    {
        private readonly RabbitMQWrapper _rabbitWrapper;

        public MessageService(
            RabbitMQWrapper rabbitWrapper
            )
        {
            _rabbitWrapper = rabbitWrapper;
            _rabbitWrapper.SendMessageToQueue(
            new ProducerSettings
            {
                ExchangeName = "PingPongExchange",
                ExchangeType = ExchangeType.Direct,
                RoutingKey = "pong"
            },
            "ping"
            );
            _rabbitWrapper.ListenQueue(
                new ConsumerSettings
                {
                    ExchangeName = "PingPongExchange",
                    ExchangeType = ExchangeType.Direct,
                    RoutingKey = "ping",
                    QueueName = "ping_queue"
                },
                OnMessageReceived
                );
        }
        private async void OnMessageReceived(object sender, BasicDeliverEventArgs args)
        {
            var body = args.Body;
            var message = Encoding.UTF8.GetString(body);
            try
            {
                Console.WriteLine($"[{DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")}] {message}!");
                await Task.Delay(2500);
                _rabbitWrapper.SendMessageToQueue(
                    new ProducerSettings
                    {
                    ExchangeName = "PingPongExchange",
                    ExchangeType = ExchangeType.Direct,
                    RoutingKey = "pong"
                    },
                    "ping"
                    );
            }
            catch (Exception e)
            {
                Console.WriteLine($"[{DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss")}] Error: {e.Message}!");
            }
            finally
            {
                _rabbitWrapper.SetAcknowledge(args.DeliveryTag, true);
            }
        }
    }
}
