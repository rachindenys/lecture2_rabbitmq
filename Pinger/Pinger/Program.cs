﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Wrapper.RabbitInfrastructure;
using System;
using Pinger.Services;

namespace Pinger
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .AddJsonFile($"appsettings.json", true, true);

            var config = builder.Build();

            var amqpConnection = config.GetSection("RabbitMQSettings:QueueUrl").Value;

            var serviceProvider = new ServiceCollection()
                .AddSingleton(x => new ConnectionFactory(new Uri(amqpConnection)).CreateConnection())
                .AddSingleton<RabbitMQWrapper>()
                .AddSingleton<IConsumer, Consumer>()
                .AddSingleton<IProducer, Producer>()
                .AddSingleton<MessageService>()
                .AddSingleton<MainService>()
                .BuildServiceProvider();

            var mainService = serviceProvider.GetService<MainService>();
            mainService.Start();

        }
    }
}
